Employee HRMS https://track.rayvat.com/  (Tracking: App (Windows, Linux, Mac) + Portal): (React, MySQL, Electron, Node, Express, Web socket, Redux saga) (Developed Windows app for tracking)

Jenya Trading Platform(Trading Services: Admin Portal) : https://stocks.rayvat.com/ (React, Express JS, MySQL, Ant Design, Web socket, Tailwind CSS, Zerodha and unstocks API, Redux saga, Multiple broker integration) 
Car Auction: (MERN: Mongo, Express js, React, Node js) (Running)

Rayvat Ecommerce (Running) (React, Express JS, MySQL, Redux saga) 

Didbiz Admin: https://admin.didbiz.com/ (React, Mysql) (Ant Design, Redux Saga, Stripe, Node API)

Didbiz User: https://panel.didbiz.com/ (React, Mysql) (Ant Design, Redux Saga, Stripe, Node API)

Didbiz Survey (VOIP Services: Portal + Site) https://www.didbiz.com/ (React, MySQL) 

Health Insurance: http://hiforme.com/ (Mysql, Express JS, React, Node JS) (Running)

Didbiz: http://app.didbiz.com/ (MERN: Mongo, Express JS, React, Node JS) (Running)

Car Bidding: https://carsandbids.com/ (MERN: Mongo, Express JS, React, Node JS)

E- Jenya (E-Attendance, Leave management, Job Apply, HR Management) Portal: http://portal.rayvat.com/ (Build With: Codeigniter, Mysql)

Tixbag (Ticket Event Services): https://tixbag.com/ (Codeigniter, Ticket Network API, Seatgeek Map) 

Tixtm (Ticket Event Services): https://tixtm.com/ (Codeigniter, Ticket Network API, Seatgeek Map)

Dialdigits Survey (VOIP Services: Portal + Site): http://104.131.72.90/dialdigits/ (Angular 8, Laravel 5.4)

Notice Ninja: http://dev.noticeninja.com/ (Laravel + React)

Rayvat: https://www.rayvat.com/ (Build With: Codeigniter, Mysql)

Rayvat Accounting (Accounting Services):  https://rayvataccounting.com/  (Build With: Codeigniter, Mysql)                 

Rayvat UK (Accounting Services): https://rayvat-uk.com/ (Build With: Codeigniter, Mysql)

Rayvat Engineering (Engineering Services): https://rayvatengineering.com/ (Build With: Codeigniter, Mysql)

CTVForMe (Cable TV Service): https://ctvforme.com/(Build With: Codeigniter, Mysql)

https://www.myaccountsconsultant.com/ (Build With: Codeigniter, Mysql)

https://www.account-consultant.com/ (Build With: Codeigniter, Mysql)

https://www.rayvatrendering.com/ (Build With: Codeigniter, Mysql)

https://www.cablepapa.com/ (Build With: Codeigniter, Mysql)

https://www.vegacadd.com/ (Build With: Codeigniter, Mysql)

https://www.creditrepairease.com/ (Build With: Codeigniter, Mysql)

https://www.creditrepairinmyarea.com/ (Build With: Codeigniter, Mysql)

https://www.axzor.com/ (Build With: Codeigniter, Mysql)

https://www.sattvdallas.com/ (Build With: Codeigniter, Mysql)

https://www.inspiquo.com/ (Build With: Codeigniter, Mysql)

https://www.igotc.com/ (Build With: Codeigniter, Mysql)

HSForMe (Home Security Service):  https://hsforme.com/ (Build With: Codeigniter, Mysql)

Ram Enterprise (Circuit Design Services): http://ramenterprise.co.in/ (Wordpress)

SATTVForMe (Satellite Services): https://www.sattvforme.com/ (Build With: Codeigniter, Mysql)
